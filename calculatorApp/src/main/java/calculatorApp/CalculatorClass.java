package calculatorApp;

class CalculatorClass {
	int number1;
	int number2;
	
	CalculatorClass(int x, int y){
		number1 = x;
		number2 = y;
	}
	
	int Add() {
		return (number1+number2);
	}
	int Subtract() {
		return number1-number2;
	}
	int Multiply() {
		return number1*number2;
	}
	float Divide() {
		return number1/number2;
	}
}
